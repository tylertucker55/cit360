import java.util.*;

/**
 * @author Tyler
 * Set demo for cit 360
 */

public class SetDemo {
    static Set<Integer> demoSet = new HashSet<>();
    static Iterator<Integer> iterator = demoSet.iterator();
    
    public SetDemo() {
        demoSet.add(0);
        demoSet.add(1);
        demoSet.add(2);
        demoSet.add(3);
        demoSet.add(4);
    }
    
    private static void executeMethods() {    
        System.out.println("-----Set-----");
        showSet();
	System.out.println("Adding 360 to the set");
	addSet(360);
        System.out.println("Adding 360 to the set again");
        addSet(360);
        System.out.println("Delete a number that isn't in the set");
	deleteFrom(225);
        System.out.println("Deleting a number");
	deleteFrom(0);
        System.out.println("Deleting the set");
	clearSet();
        System.out.println("Deleting the set (again)");
	clearSet();
    }
    
    private static void addSet(int newElement) {
	if (!demoSet.add(newElement)) {
            System.out.println("Error: "+newElement+" already exists");
	} else {
            demoSet.add(newElement);
	}
	showSet();
    }
    
    public static void deleteFrom(int delete) {
        System.out.println("Deleting "+delete);
        if (demoSet.contains(delete)) {
            demoSet.remove(delete);
            System.out.println("Success: "+delete+" has been deleted");
            showSet();
        } else {
            System.out.println("Error: "+delete+" is not in the set");
        }	
        System.out.println();
    }

    public static void clearSet() {
        System.out.println("Clearing the set");
            try {
                if(demoSet.isEmpty())
                throw new EmptyStackException();
            else {
                demoSet.clear();
                System.out.println("demoSet has been cleared");
                showSet();
            }
            } catch (EmptyStackException e) {
                System.out.println("Error: Set is empty");
            }
        System.out.println();
    }

	public static void showSet() {
            iterator = demoSet.iterator();
            while (iterator.hasNext()) {
                System.out.println(iterator.next()+" ");
            }
            System.out.println();
	}

    public static void main(String[] args) {
        new SetDemo();
        executeMethods();
    }
}