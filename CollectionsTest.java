import java.util.*;
public class CollectionsTest
{
    public static void main(String[] args)
    {

        // Create list
        List<Integer> l1
                = new ArrayList<Integer>();

        // add in some items to the index

        l1.add(0, 0);

        l1.add(1, 1);

        // Prints what is in index 0
        System.out.println("Element 0: " + l1.get(0));

        // Replace 0 index with 5
        l1.set(0, 5);

        System.out.println("List (with new element 0): ");

        System.out.println(l1);

        System.out.println("\n-----------------\n");

        // Establish a Queue
        Queue<Integer> qTest
                = new LinkedList<>();

        // Add elements to the queue
        for (int i = 1; i < 6; i++)
            qTest.add(i);

        // Display contents of the queue.
        System.out.println("Queue Elements: "
                + qTest);

        System.out.println("\n-----------------\n");

        System.out.println("Set Elements: ");

        Set<String> javaCourses
                = new HashSet<String>();

        javaCourses.add("CIT 260");
        javaCourses.add("Object-Oriented Programming");
        javaCourses.add("CIT 360");
        javaCourses.add("Object-Oriented Development");

        System.out.println(javaCourses);

        System.out.println("\n-----------------\n");

    }
}