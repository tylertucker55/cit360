/**
 * @author Tyler
 */

import java.util.*;

// Class declaration

public class TreeDemo {
    static TreeSet<Integer> myTree = new TreeSet<Integer>();
    static Iterator<Integer> iterator = myTree.iterator();
    
    public TreeDemo() {
        myTree.add(0);
        myTree.add(1);
        myTree.add(2);
        myTree.add(3);
        myTree.add(4);
        System.out.println("You added numbers!");
        System.out.println();
    }
    
    // Executes the methods involved
    
    private static void executeMethods() {
        printSet();
        firstNum();
        lastNum();
        middleNum();
        headSet();
        tailSet();
        compareSets();
        System.out.println();
        System.out.println("Number 5 added.");
        addNumber(5);
        System.out.println("Deleted number 5.");
        deleteNumber(5);
        System.out.println("Deleting number 5.");
        deleteNumber(5);
        System.out.println();
        System.out.println("Clearing...");
        clearNumbers();
        System.out.println("Clearing...");
        clearNumbers();
    }
    
    // display the tree
    private static void printSet() {
        System.out.println("Tree Set: "+myTree);
    }
    
    // display the first element 
   private static void firstNum() {
        Integer first = myTree.first();
        System.out.println("First element: "+first);
    }
   
    // display the last element
    private static void lastNum() {
        Integer last = myTree.last();
        System.out.println("Last element: "+last);
    }
    
    // display the middle elements
    private static void middleNum() {
        SortedSet<Integer> subSet = myTree.subSet(3, 7);
        System.out.println("Sub Set: "+subSet);
    }
    
    // display the head element
    private static void headSet() {
        SortedSet<Integer> headSet = myTree.headSet(5);
        System.out.println("Head Set: "+headSet);
    }
    
    // display the tail/last element
    private static void tailSet() {
        SortedSet<Integer> tailSet = myTree.tailSet(5);
        System.out.println("Tail Set: "+tailSet);
    }
    
    // display the comparator results
    private static void compareSets() {
        Comparator comparator = myTree.comparator();
        System.out.println("Comparator "+(comparator == null));
    }
    
    // display the treeset info
    private static void showNumbers() {
        iterator = myTree.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next()+" ");
        }
        printSet();
        System.out.println();
    }
    
    // adds a new element
    private static void addNumber(Integer newNumber) {
        myTree.add(newNumber);
        System.out.println("Number "+newNumber+" has been added.");
        showNumbers();
    }
    
    // deletes an element
    private static void deleteNumber(Integer deletion) {
        if(myTree.contains(deletion)) {
            myTree.remove(deletion);
            System.out.println(deletion+" is deleted.");
            showNumbers();
        } else {
            System.out.println(deletion+" does not exist.");
        }
    }
    
    // deletes the tree elements
    private static void clearNumbers() {
        try {
            if(myTree.size()==0)
                throw new EmptyStackException();
            else {
                myTree.removeAll(myTree);
                System.out.println("Treeset clear.");
                showNumbers();
            }
        } catch (EmptyStackException e) {
            System.out.println("Error: Numbers don't exist.");
        }
    }
    
    // main method to create the treedemo object and execute the methods
    public static void main(String[] args) {
        new TreeDemo();
        executeMethods();
    }
    
}