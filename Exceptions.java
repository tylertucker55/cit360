import java.util.Scanner;

public class Exceptions {

    public static void main(String[] args) {

        Scanner myObj = new Scanner(System.in);

        System.out.println("Enter two numbers to be divided:");

        try {

            int num1 = myObj.nextInt();
            int num2 = myObj.nextInt();

            int resultNum = num1 / num2;

            // return resultNum;
        }
        catch (ArithmeticException e) {
            System.out.println("You can't "
                    + "divide by zero.");
        } finally {
            System.out.println("Result: " + resultNum);
        }

    }
}