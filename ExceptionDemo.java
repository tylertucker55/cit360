import java.util.*;

public class ExceptionDemo
{
 public static void main(String[] args) throws Exception {

   Scanner scanMe = new Scanner(System.in);
        System.out.println("Please enter two numbers to divide");
        
        float num1 = 0;
        float num2 = 0;
        
        do {
            try {
                num1 = scanMe.nextFloat();
                num2 = scanMe.nextFloat();
                System.out.println("You entered: " + num1 + ", " + num2);
            }
            catch (Exception e){
                System.out.println("No 0's, please try again" + e);
            }
            
        }while(num1 == 0 | num2 == 0);
        

        calcMe(num1, num2);
 }

  public static void calcMe(float num1, float num2) throws ArithmeticException {
      
      try {
            if(num1 == 0 | num2 ==0)
                throw new EmptyStackException();
            else {
                float result = num1 / num2;
                System.out.println("The result is: " + result);
            }
        }
        catch (EmptyStackException e) {
          System.out.println("You can't have 0 as a number");
        }
 }
}