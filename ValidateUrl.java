/*
* @tyler
* Get url response code from an address in an array
*/

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class ValidateUrl {
    public static void main(String args[]) throws Exception {
        String[] urlList = { "https://postman-echo.com/", "https://www.google.com",
                "https://www.netflix.com", 
                "http://www.xasdjasdkjfa.com", "https://www.churchofjesuschrist.org"};
        for (int i = 0; i < urlList.length; i++) {
            String url = urlList[i];
            String status = getURLStatus(url);
            System.out.println(url + "\t\tStatus:" + status);
        }
    }
 
    public static String getURLStatus(String url) throws IOException {
        String result = "";
        try {
            URL siteURL = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) siteURL
                    .openConnection();
            
            connection.setRequestMethod("GET");

            connection.connect();
 
            int respCode = connection.getResponseCode();
            
            if (respCode == 200) {
                result = "Connected";
            }
            
            connection.disconnect();
        } catch (Exception e) {
            result = "Unable to reach";
        }
        return result;
    }
}