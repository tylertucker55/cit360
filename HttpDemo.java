/*
* @tyler
* http connection demo
*/

import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

public class HttpDemo {
    public static void main(String[] args) {
        try {
            
            // URL object to make url and open connection
            URL obj = new URL("http://www.google.com");
            URLConnection connectObj = obj.openConnection();
            
            Map<String, List<String>> map = connectObj.getHeaderFields();
            
            // start to print the response
            System.out.println("Response: " + obj.toString() + "\n");
            
            // get responses in a loop and print
            for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                System.out.println(entry.getKey() + " : " + entry.getValue());
            }
	
            List<String> contentLength = map.get("Content-Length");
            if (contentLength == null) {
		System.out.println("'Content-Length' is not in the URL Header!");
            } else {
                for (String header : contentLength) {
                    System.out.println("Content-Lengtht: " + header);
		}
            }
 	} catch (Exception e) {
            e.printStackTrace();
	}
    }
}