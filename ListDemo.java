/**
 * @author Tyler
 * List demonstration for cit 360
 */

import java.util.*;

public class ListDemo {
    static ArrayList<String> citCourses = new ArrayList<String>();

    private static void sort(ArrayList<String> list) {
        throw new UnsupportedOperationException("Error: Not supported."); 
    }

    public ListDemo() {
        citCourses.add("CIT171");
        citCourses.add("CIT225");
        citCourses.add("CIT240");
        citCourses.add("CIT241");
        citCourses.add("CIT262");
        System.out.println("Current courses: " + citCourses + "\n");
    }

    private static void executeMethods() {
        System.out.println("New course added:");
        addCourse("CIT270");
        System.out.println("Removed course:");
        removeCourse(5);
        System.out.println("Sorted courses:");
        sortCourses(citCourses);
        System.out.println("Removed all courses:");
        clearCourses();
    }
    
    private static void addCourse(String course) {
        citCourses.add(course);
        showCourses();
        System.out.println();
    }

    private static void removeCourse(int place) {
        int location = place;
        try {
            if(citCourses.size()==0)
                throw new Exception();
            else {
                System.out.println("Location "+location+" containing "
                        +citCourses.get(location)+" is removed.");
                citCourses.remove(location);
                showCourses();
            }
        }
        catch (Exception other) {
            System.out.println("Course at location "
                    +location+" is not there.");
        }
        System.out.println();
    }
    
    private static void sortCourses(ArrayList<String> list) {
        try {
            if(list.size()==0)
                throw new EmptyStackException();
            else {
                Collections.sort(list);
                showCourses();
                System.out.println();
            }
        }
        catch (EmptyStackException e) {
        }
    }
    
    private static void clearCourses() {
        try {
            if(citCourses.size()==0)
                throw new EmptyStackException();
            else {
                citCourses.clear();
                showCourses();
            }
        }
        catch (EmptyStackException e) {   
        }
        System.out.println("Course list is cleared. \n");
    }
    
    public static void showCourses() {
        Object course;
        try {
            if(citCourses.size()==0)
                throw new EmptyStackException();
            else {
                for(int i=0;i<=citCourses.size()-1;i++) {
                    course = citCourses.get(i);
                    if(course!=null)
                        System.out.println(course+" at specified location "+i);
                }
            }
        }
        catch(EmptyStackException e){
        }
    }
    
    public static void main(String[] args) {
        new ListDemo();
        executeMethods();   
    }
}