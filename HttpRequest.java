/*
* @tyler
* Http request
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class HttpRequest {
    public static void main(String[] args) {
        HttpURLConnection urlConnect = null;
        BufferedReader reader  = null;  
        StringBuilder builder = null;      
        OutputStreamWriter writer = null;
        String line = null;
        
        try {
            // create url
            URL urlAddress = new URL("https://jsunitcalc.glitch.me/#!/");
          
            // setup connection to the url
            urlConnect = (HttpURLConnection) urlAddress.openConnection();
          
            // get request to retrieve information
            urlConnect.setRequestMethod("GET");
          
            urlConnect.setDoOutput(true);
            
            urlConnect.setReadTimeout(5000);
                    
            // connects to the URL
            urlConnect.connect();
            
            reader  = new BufferedReader(new InputStreamReader(urlConnect.getInputStream()));
            builder = new StringBuilder();
            
            while ((line = reader.readLine()) != null) {
                builder.append(line + '\n');
            }
            System.out.println(builder.toString());
            
          // catch exceptions
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            
            // close the object
            urlConnect.disconnect();
            reader = null;
            builder = null;
            writer = null;
            urlConnect = null;
        }
    }
}