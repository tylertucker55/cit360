import java.util.*;
public class Dictionary
{
    public static void main(String[] args)
    {
        // Initializing the Dictionary
        Dictionary tyler = new Hashtable();

        // put method to get entries in the dictionary
        tyler.put("260", "programming");
        tyler.put("360", "development");

        // print out dictionary in a loop
        for (Enumeration i = tyler.elements(); i.hasMoreElements();)
        {
            System.out.println("Value in Dictionary : " + i.nextElement());
        }

        // get values for each entry
        System.out.println("\nValue at key = 360 : " + tyler.get("360"));
        System.out.println("Value at key = 260 : " + tyler.get("260"));

        // isEmpty error handling
        System.out.println("\nThere is no key-value pair : " + tyler.isEmpty() + "\n");

        // print keys
        for (Enumeration k = tyler.keys(); k.hasMoreElements();)
        {
            System.out.println("Keys in Dictionary : " + k.nextElement());
        }

        // remove entries
        System.out.println("\nRemove : " + tyler.remove("260"));
        System.out.println("Check the value of removed key : " + tyler.get("260"));

        // print size of the dictionary
        System.out.println("\nSize of Dictionary : " + tyler.size());

    }
} 