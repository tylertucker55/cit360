/**
 * @author Tyler
 * Tree demonstration for Collections assignment
 */

import java.util.*;

public class QueueDemo {
    static Queue<String> courseList = new LinkedList<>();
    Iterator<String> iterator = courseList.iterator();

    public QueueDemo() {
        courseList.add("CIT260");
        courseList.add("CIT360");
        courseList.add("CIT225");
        courseList.add("CIT325");
        System.out.println("The queue has been successfully created.\n");
    }
    
    // the method to call all methods
    
    private static void executeMethods() {
        System.out.println("-----Queue-----");
        showQueues();
        System.out.println("-----Course search-----");
        firstLine();
        System.out.println();
        System.out.println("Searching for Object-oriented Development:");
        requestQueue("CIT360");
        System.out.println();
        System.out.println("Searching for Object-oriented Programming");
        requestQueue("CIT260");
        System.out.println();
    }
    
    public static void showQueues() {
        for(Object object : courseList) {
            String element = (String) object;
            System.out.println(element);
        }
        System.out.println();
    }
    
    public static void firstLine() {
        System.out.println("First course is: "+courseList.peek()+".");
    }
    
    // requests a specific element in the queue
    
    public static void requestQueue(String course) {
        System.out.println("Requesting information about "+course+".");
        if(courseList.contains(course)) {
            System.out.println("Found "+course+".");
        } else {
            System.out.println("Could not find "+course+".");
        }
    }
    
    // main method
    
    public static void main(String[] args) {
        new QueueDemo();
        executeMethods();
    }
    
}